""" 
Usage: 
parse_movie_titles [--show]

Options:
  --show  Show all extracted locations
"""
# coding: utf-8
import spacy
import csv 
from io import StringIO
nlp = spacy.load("en_core_web_sm")
from collections import defaultdict, Counter
import pprint
pp = pprint.PrettyPrinter(indent=4)
from docopt import docopt
args = docopt(__doc__)


def parse_cvs_row(line):
    # https://stackoverflow.com/questions/2785755/how-to-split-but-ignore-separators-in-quoted-strings-in-python    
    data = StringIO(line.replace("'", "_").strip(' \n'))
    reader = csv.reader(data, delimiter=',')

    # only one line
    for row in reader:
        actor = row[-1].replace("_", "'")            
        title = row[2].replace("_", "'")
        return actor, title

    
if __name__ == '__main__':
    # don't load the entire file in memory
    lines = open('movies.csv', 'r')

    # skip header line
    next(lines)

    locations_movies = defaultdict(list)
    locations_count = Counter()
    for line in lines:
        actor, title = parse_cvs_row(line)
        doc = nlp(title)
        cities = [entity for entity in doc.ents if entity.label_=='GPE']
        if cities and args['--show']:
            print("'{}' played in movie '{}' - location '{}'".format(actor, title, cities))

        for city in set([str(city) for city in cities]) :
            locations_movies[city].extend([(actor, title)])
            locations_count.update({city: 1})

    print('\n========================================')
    for city, count in locations_count.most_common(1):
        print("Most common location is '{}'".format(city))
        print("{} movies titles contain city '{}'".format(count, city))
        pp.pprint(locations_movies[city])

