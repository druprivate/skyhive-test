# -*- coding: utf-8 -*-
import unittest
import requests
from bs4 import BeautifulSoup

################################################################################
def get_beautiful_soup_object_from_url(url):
    return BeautifulSoup(requests.get(url).text, 'html.parser')


################################################################################
def get_actors_list_wiki(url):
    actors = []
    soup = get_beautiful_soup_object_from_url(url)
    for item in soup.findAll("a", {"title" : True, "href": True, "class": False, "accesskey": False}):
        href = item.get('href')
        if href.startswith("/wiki/") and (len(href.split('/')) == 3) and not(':' in href) and not('wikipedia' in href.lower()):
            actors.append(item.get('title').split('(')[0].strip())
                
    return actors


def get_actors_actresses_wiki_first_page():
    url1 = "https://en.wikipedia.org/wiki/Category:American_male_film_actors"
    url2 = "https://en.wikipedia.org/wiki/Category:American_film_actresses"
    actors = get_actors_list_wiki(url1)
    actors.extend(get_actors_list_wiki(url2))
    return actors


def build_rt_actor_url(name):
    base_url = "https://www.rottentomatoes.com/celebrity/"
    return base_url + '_'.join(name.lower().replace('.', '').split())


################################################################################
def get_biography(soup):
    return soup.findAll("div", {"class": "celeb_summary_bio clamp clamp-5 js-clamp"})[0].text.strip()


def get_movie_credit(item):
    credit = item.findAll("td")[-3]
    credits = [e.strip() for e in credit.getText().split('\n') if e.strip()]
    credit_roles = [e.text.strip(' \n') for e in credit.findAll("em", {"class" : True})]
    
    credit_name = set(credits) - set(credit_roles) 
    producer_name = list(credit_name)[0] if credit_name else ''
    return producer_name, credit_roles


def get_movie(item):
    producer_name, credit_roles = get_movie_credit(item)
    return {"year"     : item.get("data-year"),
            "rating"   : item.get("data-rating"), 
            "title"    : item.get("data-title"), 
            "boxoffice": item.get("data-boxoffice"),
            # "credit"   : credit_roles,
            "producer" : producer_name}


def get_movies(soup, name):
    movies = []
    for item in soup.findAll("tr", {"data-boxoffice" : True}):
        movie = get_movie(item)
        movie.update({"actor": name})
        movies.append(movie)
        
    return movies


################################################################################
def get_tv_credit(item):
    credit = item.findAll("td")[-2]
    return [e.text.strip(' \n') for e in credit.findAll("em", {"class" : True})]


def get_tv_film(item):
    return {"year"     : item.get("data-appearance-year"),
            "rating"   : item.get("data-rating"), 
            # "credit"   : get_tv_credit(item), 
            "title"    : item.get("data-title")}


def get_tv_films(soup, name):
    films = []
    for item in soup.findAll("tr", {"data-appearance-year" : True}):
        film = get_tv_film(item)
        film.update({"actor": name})
        films.append(film)
        
    return films


################################################################################

class TestClass(unittest.TestCase):

    def test_get_actors_list_wiki(self):
        actors = get_actors_list_wiki(
            "https://en.wikipedia.org/wiki/Category:American_male_film_actors")

        self.assertIn("Ben Alexander", actors)
        self.assertEqual(actors[0], '50 Cent')
        self.assertEqual(actors[1], 'Lee Aaker')
        self.assertEqual(actors[-1], 'Michael J. Anderson')

    def test_build_rt_actor_url(self):
        self.assertEqual(build_rt_actor_url("50 Cent"), 'https://www.rottentomatoes.com/celebrity/50_cent')
        self.assertEqual(build_rt_actor_url('Michael J. Anderson'), 'https://www.rottentomatoes.com/celebrity/michael_j_anderson')


    def test_get_biography(self):
        text = get_biography(get_beautiful_soup_object_from_url("https://www.rottentomatoes.com/celebrity/tom_hanks"))
        self.assertIsInstance(text, str)
        self.assertEqual(text[:22], "American leading actor")
        self.assertEqual(text[-22:], "with his son (Tyler Ho")        


    def test_get_movie_credit(self):
        text = """
         <tr data-boxoffice="0" data-rating="0" data-title="Greyhound" data-year="2020">
          <td>
           <span class="tMeterIcon small noRating" data-rating="0">
            No Score Yet
           </span>
          </td>
          <td>
           <a class="unstyled articleLink" href="/m/greyhound">
            Greyhound
           </a>
          </td>
          <td>
           George Krause
           <em class="subtle">
            Screenwriter
           </em>
           <em class="subtle">
            Producer
           </em>
          </td>
          <td>
           —
          </td>
          <td>
           2020
          </td>
         </tr>
        """
        soup = BeautifulSoup(text, 'html.parser')
        self.assertEqual(get_movie_credit(soup), ("George Krause", ["Screenwriter", "Producer"]))


    def test_get_movie(self):        
        text = """
         <tr data-boxoffice="0" data-rating="0" data-title="Greyhound" data-year="2020">
          <td>
           <span class="tMeterIcon small noRating" data-rating="0">
            No Score Yet
           </span>
          </td>
          <td>
           <a class="unstyled articleLink" href="/m/greyhound">
            Greyhound
           </a>
          </td>
          <td>
           George Krause
           <em class="subtle">
            Screenwriter
           </em>
           <em class="subtle">
            Producer
           </em>
          </td>
          <td>
           —
          </td>
          <td>
           2020
          </td>
         </tr>
        """
        soup = BeautifulSoup(text, 'html.parser')
        item = soup.findAll("tr", {"data-boxoffice" : True})[0]
        self.assertEqual(get_movie(item), {'year': '2020',
                                           'rating': '0',
                                           'title': 'Greyhound',
                                           'boxoffice': '0',
                                           # 'credit': ['Screenwriter', 'Producer'],
                                           'producer': 'George Krause'})


    def test_get_tv_credit(self):
        text = """
        <tr data-appearance-year="[2019,2016]" data-rating="0" data-title="The Graham Norton Show">
         <td>
          <span class="tMeterIcon small noRating">
           No Score Yet
          </span>
         </td>
         <td>
          <a class="articleLink unstyled" href="/tv/the_graham_norton_show">
           The Graham Norton Show
          </a>
          <div class="subtle">
           2007
          </div>
         </td>
         <td>
          <em class="subtle">
           Guest
          </em>
         </td>
         <td>
          <ul>
           <li>
            <span>
             2019
            </span>
           </li>
           <li>
            <span>
             2016
            </span>
           </li>
          </ul>
         </td>
        </tr>
        """
        soup = BeautifulSoup(text, 'html.parser')
        self.assertEqual(get_tv_credit(soup), ['Guest'])

        
    def test_get_tv_film(self):
        text = """
        <tr data-appearance-year="[2019,2016]" data-rating="0" data-title="The Graham Norton Show">
         <td>
          <span class="tMeterIcon small noRating">
           No Score Yet
          </span>
         </td>
         <td>
          <a class="articleLink unstyled" href="/tv/the_graham_norton_show">
           The Graham Norton Show
          </a>
          <div class="subtle">
           2007
          </div>
         </td>
         <td>
          <em class="subtle">
           Guest
          </em>
         </td>
         <td>
          <ul>
           <li>
            <span>
             2019
            </span>
           </li>
           <li>
            <span>
             2016
            </span>
           </li>
          </ul>
         </td>
        </tr>
        """
        soup = BeautifulSoup(text, 'html.parser')
        item = soup.findAll("tr", {"data-appearance-year" : True})[0]
        self.assertEqual(get_tv_film(item), {'year': '[2019,2016]',
                                             'rating': '0',
                                             # 'credit': ['Guest'],
                                             'title': 'The Graham Norton Show'
                                             })
        
        
################################################################################        
if __name__ == "__main__":
    unittest.main()
