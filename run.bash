#!/bin/bash

# create virtual environment
pip install virtualenv
virtualenv venv

# install dependencies
pip install -Uvr requirements.txt
python -m spacy download en_core_web_sm

# unit tests
python functions.py

# scrap rottentomatoes
rm movies.csv
scrapy runspider romatoesbot.py -o movies.csv -t csv

# extract actors and movies corresponding to most frequent location
python parse_movie_titles.py

# exit virtualenv
deactivate

exit 0
