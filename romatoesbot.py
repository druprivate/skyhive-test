# -*- coding: utf-8 -*-
import scrapy
import requests
from bs4 import BeautifulSoup
from functions import (get_actors_actresses_wiki_first_page, build_rt_actor_url, get_movie)

################################################################################
actors_names_list = get_actors_actresses_wiki_first_page()
url_to_names = {build_rt_actor_url(name) : name for name in actors_names_list}

################################################################################
class RomatoesbotSpider(scrapy.Spider):
    name = 'romatoesbot'
    allowed_domains = ['https://www.rottentomatoes.com/celebrity/']
    start_urls = list(url_to_names.keys()) # [:5]

    def parse(self, response):
        current_url = response.request.url
        name = url_to_names[current_url]
        soup = BeautifulSoup(response.text, 'html.parser')
        # biography = get_biography(soup)
        # get_movies(soup, name)
        # tv_films = get_tv_films(soup)

        for item in soup.findAll("tr", {"data-boxoffice" : True}):
            movie = get_movie(item)
            movie.update({"actor": name})
            yield movie
        
